const http = require('http');
const speech = require('@google-cloud/speech');
const fs = require('fs');

const client = new speech.SpeechClient();

const fileName = './resources/test.wav';
const file = fs.readFileSync(fileName);
const audioBytes = file.toString('base64');

const audio = {
    content: audioBytes,
  };

  const config = {
    encoding: 'LINEAR16',
    languageCode: 'en-US',
    model: 'default',
  };
  const request = {
    audio: audio,
    config: config,
  };

http.createServer(function (req, res) {
    client.recognize(request).then(data => {
        const response = data[0];
        const transcription = response.results.map(result => result.alternatives[0].transcript).join('\n');
        console.log(`Transcription: ${transcription}`);
        res.write(`Transcription: ${transcription}`); 
        res.end(); 
    }).catch(err => {
        console.error('ERROR:', err);
        res.write('ERROR:', err); 
        res.end(); 
    });
  }).listen(9000);  